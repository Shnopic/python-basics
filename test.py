#!/bin/python

a=str(input())
b=int(input())

def path_read(c: str):
    file1=open(f"{c}", "r")
    return file1.read()

def path_write(c: str,d: int):
    file1=open(f"{c}", "r+")
    file1.write(f"{d+1} \n")
    return "Success \n"

print()
print(path_read(a))
print(path_write(a,b))
print(path_read(a))

